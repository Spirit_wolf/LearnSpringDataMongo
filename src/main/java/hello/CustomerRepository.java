package hello;

import java.util.List;

import hello.pojos.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerRepository extends MongoRepository<Customer, String> {

    Customer findByFirstName(String firstName);
    List<Customer> findByLastName(String lastName);

    Customer findByFirstNameIgnoreCaseAndLastNameIgnoreCase(String firstName,String lastName);
    Customer findByFirstNameAndLastNameAllIgnoreCase(String firstName,String lastName);

}
