package hello.dto;

import hello.pojos.Goods;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

/**
 * @author Spirit_wolf
 * @date 2018/10/26
 */
@Document(collection = "customer")
public class OneTypeBagDTO {
    private Map<Long, Goods> goodsGuid2Goods = new Long2ObjectOpenHashMap<>();

    public Map<Long, Goods> getGoodsGuid2Goods() {
        return goodsGuid2Goods;
    }

    public void setGoodsGuid2Goods(Map<Long, Goods> goodsGuid2Goods) {
        this.goodsGuid2Goods = goodsGuid2Goods;
    }
}
