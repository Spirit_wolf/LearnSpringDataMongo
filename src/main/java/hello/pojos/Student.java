package hello.pojos;

import org.springframework.data.annotation.Id;

import java.util.List;

/**
 * Created by designerpc on 2018/12/31.
 */
public class Student {
    @Id
    private String id;

    public static class InnerClass {
        private int grade;
        private int mean;
        private int std;

        public InnerClass() {
        }

        public InnerClass(int grade, int mean, int std) {
            this.grade = grade;
            this.mean = mean;
            this.std = std;
        }
    }

    private List<InnerClass> grades;

    public Student() {
    }

    public Student(List<InnerClass> grades) {
        this.grades = grades;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<InnerClass> getGrades() {
        return grades;
    }

    public void setGrades(List<InnerClass> grades) {
        this.grades = grades;
    }
}
