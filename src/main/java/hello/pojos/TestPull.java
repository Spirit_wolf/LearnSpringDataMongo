package hello.pojos;

import org.springframework.data.annotation.Id;

import java.util.List;

/**
 * Created by gzc on 2018/12/30.
 */
public class TestPull {
    @Id
    private String id;

    private String name;

    private List<InnerClass> tags;

    public static class InnerClass{
        private String tag;
        private int time;

        public InnerClass() {
        }

        public InnerClass(String tag, int time) {
            this.tag = tag;
            this.time = time;
        }

        public String getTag() {
            return tag;
        }

        public void setTag(String tag) {
            this.tag = tag;
        }

        public int getTime() {
            return time;
        }

        public void setTime(int time) {
            this.time = time;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<InnerClass> getTags() {
        return tags;
    }

    public void setTags(List<InnerClass> tags) {
        this.tags = tags;
    }
}
