package hello.pojos;


import hello.enums.BagType;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.EnumMap;
import java.util.Map;

/**
 * @author Spirit_wolf
 * @date 2018/10/25
 */
@Document(collection = "customer")
public class BagInfo {

    private Map<BagType, Map<Long, Goods>> type_goodsGuid2Goods = new EnumMap<>(BagType.class);

    public Map<BagType, Map<Long, Goods>> getType_goodsGuid2Goods() {
        return type_goodsGuid2Goods;
    }

    public void setType_goodsGuid2Goods(Map<BagType, Map<Long, Goods>> type_goodsGuid2Goods) {
        this.type_goodsGuid2Goods = type_goodsGuid2Goods;
    }
}
