package hello.pojos;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;

import java.util.Set;

/**
 * @author Spirit_wolf
 * @date 2018/10/25
 */
public class OpenServerActivityInfo {
    //用Set<Integer>是OK的，用IntSet则不行
    private final Set<Integer> awardSet = new IntOpenHashSet();

    public Set<Integer> getAwardSet() {
        return awardSet;
    }
}
