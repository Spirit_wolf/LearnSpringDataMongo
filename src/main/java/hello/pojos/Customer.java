package hello.pojos;

import hello.enums.Sex;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.ArrayList;
import java.util.List;


public class Customer {

    @Id
    public String id;

    public String firstName;
    public String lastName;

    public Sex sex;
//    @DBRef
    private final BagInfo bagInfo = new BagInfo();

    private final List<Mail> mailList = new ArrayList<>();

    private final OpenServerActivityInfo openServerActivityInfo = new OpenServerActivityInfo();

    public Customer() {}

    public Customer(String firstName, String lastName, Sex sex) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.sex = sex;
    }

    @Override
    public String toString() {
        return String.format(
                "Customer[id=%s, firstName='%s', lastName='%s', sex=%s]",
                id, firstName, lastName, sex);
    }

    public BagInfo getBagInfo() {
        return bagInfo;
    }

    public List<Mail> getMailList() {
        return mailList;
    }

    public OpenServerActivityInfo getOpenServerActivityInfo() {
        return openServerActivityInfo;
    }
}

