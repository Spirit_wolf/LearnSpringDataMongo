package hello.pojos;

/**
 * @author Spirit_wolf
 * @date 2018/10/25
 */
public class Mail {
    private int mailId;
    private String title;
    private String context;

    public Mail(int mailId, String title, String context) {
        this.mailId = mailId;
        this.title = title;
        this.context = context;
    }

    public int getMailId() {
        return mailId;
    }

    public void setMailId(int mailId) {
        this.mailId = mailId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }
}
