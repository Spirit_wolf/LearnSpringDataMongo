package hello.pojos;

/**
 * @author Spirit_wolf
 * @date 2018/10/25
 */
public class Goods {
    private int goodsId;
    private int goodsNum;
    private long goodsGuid;

    public Goods(int goodsId, int goodsNum, long goodsGuid) {
        this.goodsId = goodsId;
        this.goodsNum = goodsNum;
        this.goodsGuid = goodsGuid;
    }

    public int getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(int goodsId) {
        this.goodsId = goodsId;
    }

    public int getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(int goodsNum) {
        this.goodsNum = goodsNum;
    }

    public long getGoodsGuid() {
        return goodsGuid;
    }

    public void setGoodsGuid(long goodsGuid) {
        this.goodsGuid = goodsGuid;
    }

    @Override
    public String toString() {
        return "Goods{" +
                "goodsId=" + goodsId +
                ", goodsNum=" + goodsNum +
                ", goodsGuid=" + goodsGuid +
                '}';
    }
}
