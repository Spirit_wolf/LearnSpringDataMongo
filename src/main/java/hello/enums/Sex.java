package hello.enums;

/**
 * @author Spirit_wolf
 * @date 2018/10/25
 */
public enum Sex {
    man,
    woman,
}
